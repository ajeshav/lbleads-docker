# Docker Setup for Leads Manage Portal

### Containers
- PHP-fpm:5.6
- MySQL:5.5
- nginx

## Setup instructions

* Checkout Leads Repo

``` git clone git@bitbucket.org:litebreeze/leads.git```

``` cd leads```

```git submodule add git@bitbucket.org:ajeshav/lbleads-docker.git docker```



Now the project will have the following file structure

```
| leads
|- - app
|- - config
|- - .....
|- - public_html
|- - docker
|- - - - .git
|- - - - Dockerfile
|- - - - default.conf
|- - - - docker-compose.yml
```

``` cd docker```

``` docker-compose build ```

``` docker-compose up -d ```

``` docker ps ```

The following containers will be up and running

* lbleads-web
* lbleads-db
* lbleads-php

### Now setup the project

Update the MySQL database details as

```
            'host'      => 'lbleads-db',
			'database'  => 'lbleads_local',
			'username'  => 'lbleadsuser',
			'password'  => 'lbleadsuser',
```

Composer install & running migrations

``` docker-compose exec lbleads-php /bin/bash ```

inside the container

``` composer install ```

from ```leads/docker``` folder

``` docker-compose exec lbleads-php php artisan migrate ```


### Connecting database with SequelPro

```
    host : 127.0.0.1
    port: 3389
    user: lbleadsuser
    password: lbleadsuser
    database: lbleads_local
```





